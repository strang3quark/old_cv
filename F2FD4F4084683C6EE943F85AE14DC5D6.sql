-- MySQL dump 10.13  Distrib 5.6.47, for Linux (x86_64)
--
-- Host: localhost    Database: heroku_e3a47fd0ff1008a
-- ------------------------------------------------------
-- Server version	5.6.47-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blog_categories`
--

DROP TABLE IF EXISTS `blog_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_categories` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_categories`
--

LOCK TABLES `blog_categories` WRITE;
/*!40000 ALTER TABLE `blog_categories` DISABLE KEYS */;
INSERT INTO `blog_categories` VALUES (1,'Programming'),(2,'Linux'),(3,'Others');
/*!40000 ALTER TABLE `blog_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_posts`
--

DROP TABLE IF EXISTS `blog_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(120) NOT NULL,
  `author` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `content` varchar(10000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_posts`
--

LOCK TABLES `blog_posts` WRITE;
/*!40000 ALTER TABLE `blog_posts` DISABLE KEYS */;
INSERT INTO `blog_posts` VALUES (1,'Hello World',1,'2016-11-30 17:20:00','Hey guys, this is my new Blog, this isn\'t Wordpress or anything similar, I did it \"from scratch\" with CodeIgniter for the backend and Bootstrap for the frontend.\r\n\r\nThe entire website (including portfolio) was build with CodeIgniter, all the fields are dynamic and stored in a MySQL database. The articles are written in markdown ([CI-Markdown](https://github.com/jonlabelle/ci-markdown \"CI-Markdown\")) and then stored in the database.\r\n\r\nThe next step will be the integration of a feedback system (probably Disqus) and subsequently building a back-office.\r\n\r\nThere\'s a lot of things to be done in this website, such as:\r\n\r\n - Organize article list in pages.\r\n - Back-office for editing the About Me page and blog posts.\r\n - Comment system (probably disqus).\r\n - Search for articles.\r\n\r\nI\'m currently using GIT to track development changes, I will share the code in GitHub as soon as I get the tasks mentioned above done.\r\n\r\nI\'m still uncertain about what will be my future posts in here, I\'m thinking about some video tutorials about programming, but I still have to think about it.\r\n\r\nAs you may have noticed my english writing is far from perfect, I\'m not a native english speaker and I am doing my best to make this readable, if you have some suggestions or noticed some mistake e-mail me at [bruno.fl.jesus@gmail.com](mailto:bruno.fl.jesus@gmail.com).\r\n\r\nThat\'s all, thank you for reading!'),(5,'Fix: wrong colors on Raspberry Pi with i3-wm',1,'2016-11-30 18:03:00','I had a Raspberry Pi laying around I today I decided to install Arch Linux on it with a light and simple Window Manager.\n\nI also use i3wm on my laptop so I\'ve used the same configuration with only small changes, reloaded i3wm and **surprise**:\n \n  - **Instead of my gray bar and orange window borders I\'ve got a cyan bar and green window borders!**\n\nIt was an ugly mess, I found it strange because the terminal colors were fine, the same for GUI apps, only i3 had that weird behavior.\n\nAfter some research I managed to find a solution, just add this to `/boot/config.txt`:\n\n    framebuffer_depth=32\n    framebuffer_ignore_alpha=1\n    framebuffer_swap=1\n    \n\nIf you want to get some details about the problem you can read [this](http://infra.in.zekjur.net/pipermail/i3-discuss/2014-August/001827.html)'),(6,'Converting PDF files to HTML',1,'2016-12-06 15:15:00','It\'s a shame that we don\'t still have a decent PDF reader in Linux, I was in need of a PDF reader that let me copy and paste without messing all the formatting (the Firefox PDF reader didn\'t worked well) and I didn\'t want to install a PDF reader with lots of dependencies, I like things simple, that\'s why I don\'t use any Desktop Environment, just i3-wm, simple applications, some scripts I\'ve been made and other scripts that I find online. \n\nI\'ve tried XPDF, it\'s a decent PDF reader but the copy and paste operation is a little weird.\n\nI was tired of PDF readers and I needed a solution, that\'s when I found **poppler**, it\'s a PDF renderer who let\'s you convert PDF files to other formats.\n\nYou can install **poppler** package in Arch Linux with `pacman -S poppler`.\n\nI\'ve made a simple script that uses poppler to convert the file to HTML, saves it in `/tmp/<filename>` and then opens it in your default browser.\n\n    #! /bin/bash\n    # convert_pdftohtml.sh\n    # Copyright (C) 2016 Bruno Jesus (aka strang3quark) <bruno.fl.jesus@gmail.com>\n    #\n    # Distributed under terms of the MIT license.\n    #\n    \n    PDFPATH=$1;\n    PDFFILE=$(basename $PDFPATH);\n    \n    \n    if [ \"$2\" == \"--format\" ]; then\n    	FORMAT=\"-s\";\n    	HTMLFILE=\"index-html.html\";\n    else\n    	FORMAT=\"\";\n    	HTMLFILE=\"index.html\";\n    fi\n    \n    mkdir /tmp/$PDFFILE;\n    \n    pdftohtml -p $FORMAT $PDFPATH /tmp/$PDFFILE/index.html\n    \n    $BROWSER /tmp/$PDFFILE/$HTMLFILE\n\n\n----------\n\n\n**The usage is very simple:**\n\n`convert_pdftohtml.sh myfile.pdf` - this will remove all the weird formatting\n\n`convert_pdftohtml.sh myfile.pdf --format` - this will keep all the formatting\n\nIf you have some alternatives or suggestions please contact me.\n'),(7,'Automatically start Virtual Machine and auto-mount',1,'2016-12-09 16:40:00','I love Virtual Machines, they are useful and with them you can do whatever you want without cluttering your main system.\n\nI like to keep my main os as simple as possible, if I want some server software I usually create a Virtual Machine for that.\n\nMy workflow to start working with web development was something like this:\n\n1. Open VirtualBox\n2. Start the virtual machine\n3. Mount the /var/www in the Host OS with SSHFS\n4. Open VIM and start working\n\nI had an alias to mount the /var/www directory but that isn\'t enough, I still had to open VirtualBox, start the virtual machine and I had to keep the Virtual Machine window open.\n\nI made a simple script to automate that, plus the VM runs in headless mode, all you need is VirtualBox, a virtual machine and SSHFS.\n\n**The script:**\n\n    #! /bin/sh\n\n    ACTION=$1\n    VM_NAME=$2;\n    \n    if [ \"$ACTION\" == \"start\" ]; then\n    	VM_USER=$3;\n    	VM_IP=$4;\n    	VM_PATH=$5;\n    	LOCAL_PATH=$6;\n    	PASSWORD=$7;\n    	BOOT_WAIT=$8;\n    \n    	VBoxManage startvm \"$VM_NAME\" --type headless\n    	sleep $BOOT_WAIT\n    	echo \"$PASSWORD\" | sshfs $VM_USER@$VM_IP:$VM_PATH $LOCAL_PATH -o password_stdin\n    	echo \"DONE\"\n    	exit\n    fi;\n    \n    if [ \"$ACTION\" == \"stop\" ]; then\n    	LOCAL_PATH=$3;\n    	fusermount -u $LOCAL_PATH\n    	VBoxManage controlvm \"$VM_NAME\" acpipowerbutton\n    	echo \"DONE\"\n    	exit\n    fi;\n    \n    # no action provided or invalid, show usage\n    echo \"Start Virtual Machine and automount\"\n    echo \"Usage: vm_mount.sh <action> <vm_name> <vm_user> <vm_ip> <vm_addr> <local_mountpoint> <vm_password> <wait_for_boot_time_in_seconds>\"\n    echo \"Examples:\"\n    echo \'	vm_mount.sh start \"TURNKEY LAMP\" root vmlamp.local /var/www ~/vmwww password 10\'\n    echo \'	vm_mount.sh stop \"TURNKEY LAMP\" ~/vmwww\'\n\n \n**You can download with curl using:**\n\n`curl ix.io/1Lbu > vm_start.sh && chmod +x vm_start.sh`\n\nThe usage of this script is very simple, just call `./vm_mount.sh --help` and it will print some examples.\n\n**Tip:** Create an alias for each of your virtual machines, this way you don\'t need to type all those arguments. You can do that in .bashrc or .zshrc, (...) depending of your shell.\n\nBy the way, [ix.io](ix.io) is a great tool, you should take a look at it.\n\nAs always you can contact me if you have some suggestions. Thank you for reading.\n\n'),(8,'Set default user and group for new files/directories',1,'2016-12-16 15:30:00','Hey guys, today I faced a problem, I wanted my new /var/www files to be owned by `www-data` user and `www-data` group, sure I can `chown` manually every time I create new files, but that wasn\'t the solution I was looking for.\n\nI think ACL might be a possible solution but I know nothing about that and I needed a fast solution.\n\n## The reason\n\nI\'m running some bots on a virtual machine, these bots crawl some websites then write the captured content inside `/var/www`.\n\nYou may not want to apply the contents of this article on a production server, the apache user (www-data) should only have read access to the website content, this way if apache or your website have some vulnerability the website content and the server will be more \"secure\".\n\n## The solution\n\nAfter some searching I found `incron`, as said in the [official page](http://inotify.aiken.cz/?section=incron&page=about&lang=en):\n\n> This program is an \"inotify cron\" system. It consists of a daemon and a table manipulator. You can use it a similar way as the regular cron. The difference is that the inotify cron handles filesystem events rather than time periods. \n\nThat seems to be all I was looking for, except for one little problem, it\'s not recursive! This means that if I\'m watching `/var/www/` and I create a file/directory in `/var/www/mypage/` it won\'t do anything.\n\n**After some more searching I finally found something, [Watcher](https://github.com/DeepSpace2/Watcher):**\n> Watcher is a daemon that watches specified files/folders for changes and fires commands in response to those changes. It is similar to incron, however, configuration uses a simpler to read ini file instead of a plain text file. Unlike incron it can also recursively monitor directories. It\'s also written in Python, making it easier to hack. \n\nThis is a fork of [this](https://github.com/splitbrain/Watcher), I chose the fork because it\'s written for Python 3.\n\n### Let\'s get our hands dirty\nFirst of all, I tested this on a machine running debian, I won\'t cover Arch Linux this time, It should be easy to get it running on Arch except for the cron path but you can achieve the same with a systemd service.\n\n 1. First we need to choose where we want to store the software, I chose the `/opt` directory:\n\n `$ cd /opt`\n\n 2. Now we clone the repository (download the script):\n\n `$ git clone https://github.com/splitbrain/Watcher`\n\n   \n 3. We need python and pyinotify, if you are using `debian` execute the following:\n\n `# apt-get install python python-pyinotify`\n \n 4. Go to `/opt/Watcher` directory:\n\n `$ cd /opt/Watcher`\n\n 5. Change the contents of the configuration (`/opt/watcher.ini`) file to this:\n     \n        [DEFAULT]\n        logfile=/tmp/watcher.log\n        pidfile=/tmp/watcher.pid\n        \n        [job1]\n        watch=/var/www\n        events=create\n        excluded=\n        recursive=true\n        autoadd=true\n        command=chown www-data:www-data -R $filename\nI have removed all the commented lines to ease the readability, you probably want to store the sample config file for future reference.\n\n 6. Now try to run the watcher in debug mode for testing:\n\n `$ ./watcher.py debug -c watcher.ini`\n\n 7. If it\'s all working you can kill it with `Ctrl+Z`\n 8. You probably want this to start on boot, we can achieve this with cron, to do that execute the following:\n\n `# crontab -e`\n \n 9. With the editor running you just need to add this line in the end of the file:\n\n `@reboot /opt/Watcher/watcher.py -c /opt/Watcher/watcher.ini start`\n \n**Note:** If the editor is `vi` you can enter insert mode with the `i` key, after the edition just press `ESC` and then `:wq`.\n\nAnd that\'s it, you can reboot and test if it\'s working.\n\n***\n\nEdited on 19/12/2016, added security warning. Thanks [z3bra](http://z3bra.org)\n'),(9,'Reading feeds in the terminal',1,'2016-12-26 01:30:00','I live in the terminal and I also like to read my feeds there.\n\nFor a long time I used [newsbeuter](http://newsbeuter.org/), it was ok, but I didn\'t like to much and I also wanted to learn how to create nCurses programs, that\'s why I created [simpleRSS](https://github.com/strang3quark/simpleRSS).\n\nIt\'s far from finished and certanly far from perfect but it\'s pretty usable right now.\n\nThe feeds can be organized by categories and you can switch through the menus with h,j,k,l just like in vim.\n\n**Feeds Screen**\n\n![Feeds Screen](https://camo.githubusercontent.com/2f15dae1b42b7be8d0166dc51622bf6950362c26/687474703a2f2f692e696d6775722e636f6d2f465050306f70612e706e67)\n\n\n**Articles List**\n\n![Articles List](https://camo.githubusercontent.com/b227748ee1e234277d46212e2e409342640bfd5e/687474703a2f2f692e696d6775722e636f6d2f3133437963744e2e706e67)\n\n\n**Article Reading / Links**\n\n![Article Links](https://camo.githubusercontent.com/b4d7f08ed05d814516602dadcbfe4685eed93936/687474703a2f2f692e696d6775722e636f6d2f506c68766a486e2e706e67)\n\nI plan to support image display with `w3mimgdisplay` like [ranger](http://ranger.nognu.org):\n\n![Ranger Image Support](http://ranger.nongnu.org/screenshots/w3mimgpreview.png)\n\nBut before that I still need to do a major code revision (that code is a mess) and also implement some features like: \n\n - Logs (for debug mode)\n - Bookmarks\n - Export articles to Markdown or HTML.\n\nIf you are interested in using it or contributing take a look [here](https://github.com/strang3quark/simpleRSS).\n'),(10,'Desktop Tour',1,'2017-01-06 17:30:00','This time I will show you my desktop, it\'s not perfect but I\'m productive with it.\n\n**Let\'s start with a clean workspace**\n\n![Clean Workspace](http://strang3quark.cf/article_images/20161231_desktoptour/clean_workspace.png)\n\nAs you can see it looks pretty simple, a top bar with the information and a clean desktop.\nThe bar you see on top is i3bar, the content in the bar is provided by a shell script I made.\n\n---\n\n**Fake busy**\n\n![Fake Busy](http://strang3quark.cf/article_images/20161231_desktoptour/fake_busy.png)\n\nAs you may noticed I use a tilling window manager, i3.\nThere\'s a floating window with screenfetch with some details on my configuration.\n\nI use **zsh** as my shell, bash is fine too but there are a lot of nice plugins for zsh.\n\nSoftware in this screenshot:\n\n - Weechat - IRC client\n - SimpleRSS - RSS Reader (Check my previous post for more info)\n - Ranger - A cool File Manager with image support\n\n\nOther terminal softwares I use:\n\n - Cgo - Gopher Browser\n - Vim - Text Editor (Mostly for development)\n - Mutt - For e-mails\n - w3m - For some webbrowsing\n - Scripts, a lot of scripts!\n\n**The Browser - Firefox**\n\n![Fake Busy](http://strang3quark.cf/article_images/20161231_desktoptour/vimperator.png)\n\nI use firefox with **vimperator** and **stylish** (yes, I am a Vim addict).\n\nMy Firefox theme is a slightly modified version of [Twilly Firefox.css](http://twily.info/firefox/stylish/firefox-css)\n\n---\n\nI will update my dotfiles on github and then I will make a post about my Vim configuration.\n\n**Happy new year**');
/*!40000 ALTER TABLE `blog_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_posts_categories`
--

DROP TABLE IF EXISTS `blog_posts_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_posts_categories` (
  `rid` int(3) NOT NULL AUTO_INCREMENT,
  `post` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  PRIMARY KEY (`rid`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COMMENT='RelationTable';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_posts_categories`
--

LOCK TABLES `blog_posts_categories` WRITE;
/*!40000 ALTER TABLE `blog_posts_categories` DISABLE KEYS */;
INSERT INTO `blog_posts_categories` VALUES (1,1,3),(2,5,2),(3,6,1),(4,6,2),(5,7,1),(6,7,2),(7,8,2),(8,9,2),(9,10,2);
/*!40000 ALTER TABLE `blog_posts_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `education`
--

DROP TABLE IF EXISTS `education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `education` (
  `id_education` int(11) NOT NULL AUTO_INCREMENT,
  `course` varchar(80) NOT NULL,
  `institution` varchar(60) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  `start` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  PRIMARY KEY (`id_education`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `education`
--

LOCK TABLES `education` WRITE;
/*!40000 ALTER TABLE `education` DISABLE KEYS */;
INSERT INTO `education` VALUES (1,'Professional Course of Systems Management and Programming','Escola Secundária de Peniche','Level 4 Course. Learned some basic programming in Pascal, C and .NET, also learned some network basics.','2009-09-01','2012-07-01'),(2,'Especialization Course of Network Planning and Systems Management','Escola Superior de Gestão e Técnologia  de Santarém','Level 5 Course. Especialization in Networks, learned about active and passive network equipment and server configuration in Windows and Linux.','2013-10-01','2014-07-01'),(3,'Computer Science Degree','Escola Superior de Gestão e Técnologia de Santarém','Level 6 Course. Learned more advanced programing techniques like OOP. I got some interesting skills like JSP, HTML5, Android Development and ASP.NET, I also learned more about Git and PHP.','2014-10-01','2017-10-01');
/*!40000 ALTER TABLE `education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolio_categories`
--

DROP TABLE IF EXISTS `portfolio_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portfolio_categories` (
  `id_pcategory` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id_pcategory`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Categories for Portfolio Items';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolio_categories`
--

LOCK TABLES `portfolio_categories` WRITE;
/*!40000 ALTER TABLE `portfolio_categories` DISABLE KEYS */;
INSERT INTO `portfolio_categories` VALUES (2,'Personal'),(1,'Work');
/*!40000 ALTER TABLE `portfolio_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolio_projects`
--

DROP TABLE IF EXISTS `portfolio_projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portfolio_projects` (
  `id_project` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description_short` varchar(45) NOT NULL,
  `description_long` varchar(1500) NOT NULL DEFAULT '',
  `release` date NOT NULL,
  `link` varchar(85) DEFAULT NULL,
  PRIMARY KEY (`id_project`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolio_projects`
--

LOCK TABLES `portfolio_projects` WRITE;
/*!40000 ALTER TABLE `portfolio_projects` DISABLE KEYS */;
INSERT INTO `portfolio_projects` VALUES (1,'Electronic Keylocker','Python','<p><strong>Key access made secure and convenient</strong></p>\n<p>With this solution your company will keep track of all keys in a easy way!</p>\n<ul>\n<li>Highly customizable access control with key groups, user groups and schedules</li>\n<li>Alarm support (Highly configurable)<ul>\n<li>Forced door open</li>\n<li>Logged out and left the door open</li>\n<li>Key returned without login</li>\n<li>Key forcibly removed without login</li>\n<li>Key forcibly removed by user</li>\n<li>Power Failure</li>\n</ul>\n</li>\n<li>All events are logged</li>\n<li>Schedules for keys and users</li>\n</ul>\n\n<p><strong>Roles:</strong> Operating System configuration, software development and database planning.</p> ','2013-03-01','http://www.redislogar.pt/pt/solucoes/chaveiros/'),(2,'Keylocker Backoffice','Python/PyQT','<p><strong>Configure your RedisKey equipments from your computer</strong></p>\n<p>This software makes it possible to configure RedisKey more easily and remotely.</p>\n<ul>\n<li>Pre-Register keys</li>\n<li>Configure schedules</li>\n<li>Configure user permissions</li>\n<li>See who has the keys</li>\n<li>See what keys are with one specific user</li>\n<li>See and export all logs</li>\n<li>Configure language, alerts...</li>\n<li>Backup and Restore the Keylocker</li>\n</ul>\n\n<p><strong>Roles:</strong> Software design and development.</p> ','2013-07-01','http://www.redislogar.pt/pt/solucoes/chaveiros/'),(3,'Call Center CRM','PHP/HTML/CSS','<p><strong>Customer Relationship Manager for Call Center</strong></p>\n<ul>\n<li>Manage clients and track their changes over time</li>\n<li>Tracks calls, meetings and comunications results</li>\n<li>Organized by teams with team leaders and company managers\n<ul>\n<li>Team Leaders can assign contacts and tasks for they members</li>\n<li>Team Leader can view and edit all contacts for they team</li>\n</ul>\n</li>\n<li>Responsive design with a great smartphone experience</li>\n</ul>\n\n<p><strong>Roles:</strong> Developer (Frontend, Backend and Database)</p>','2014-06-01',NULL),(4,'simpleRSS','Python/nCurses','<p><strong>Read your feeds without using a graphical interface</strong></p>\n<p>SimpleRSS allows you to read feeds from your terminal in an easy way.</p>\n<p>I started doing this project to learn how to work with ncurses, this is inspired in newsbeuter (witch is a lot better than simpleRSS).</p>\n<ul>\n<li>Compatible with RSS/Atom</li>\n<li>Article content is shown in Markdown</li>\n<li>All feeds are saved to a SQLite database to allow offline reading</li>\n<li>Easy to configure through a config file</li>\n<li>Vim keys for navigation (h,j,k,l)</li>\n</ul>\n<p>I did this just for learning proposes and I don\'t plan to develop it further, you are free to continue the development.</p> \n\n','2015-08-01','https://gitlab.com/strang3quark/simpleRSS'),(5,'Locker Backoffice','Python/PyQT','<p><strong>Real time remote locker management</strong></p>\n<p>This was the first backoffice software that I had made, it is fully programmed in Python using PyQT.</p>\n<ul>\n<li>Check the status of the coin acceptor and the currently used lockers</li>\n<li>View logs and transactions</li>\n<li>Change price table and machine screens</li>\n</ul>\n\n<p><strong>Roles:</strong> Software design and development.</p>','2012-06-01','http://www.redislogar.pt/pt/solucoes/cacifos/'),(6,'RemoteDroid','Java/Javascript/HTML/CSS','<p><strong>Send and Receive SMS from your smartphone using your computer</strong></p>\n<p>This project consists on a REST API and an embedded JavaScript Client.</p>\n<ul>\n<li>The code is available on my GitHub page under GPL v3 license</li>\n</ul>\n\n<p><strong>Roles:</strong> Android and Web Development.</p>\n<p><strong>Technologies Used:</strong> Java, HTML, CSS, Javascript</p>','2017-01-20','https://gitlab.com/strang3quark/remotedroid'),(8,'PriceTracker','Angular/Spring/Hibernate','<p><strong>Tracks prices of products in various stores.</strong></p>\n<p>You can check if the product is worth buying by analyzing the price changes over time.</p>\n<p>The software also searches for the product on other stores, helping you find the better deal.</p>\n<hr>\n<p>The first version was written in <b>PHP</b> with a custom <b>MVC</b> framework, I later moved to <b>CodeIgniter</b>.</p>\n<p>I\'m now developing the Pricetracker using <b>Java</b> with <b>Spring Framework</b>, I chose <b>PostgreSQL</b> for the database and I\'m using <b>Hibernate</b>, which means I could switch to another database (MySQL, Oracle, etc) with minimum effort, also the database changes are versioned with <b>Liquibase</b>.</p>\n<p>Every store is an add-on written in JavaScript, the scripts are interpreted on runtime with <b>Nashorn</b>, this means that stores can be added by installing plugins, without needing to mess with the application code and compiling everything again.</p>\n<p>This software also uses <b>task scheduling</b>, this jobs can be configured in many ways, even the number of running threads per job.</p>','2017-04-20','https://pricetracker.pt'),(7,'HomePaq','Python/Linux','<p><strong>Electronic Lockers for Post Services</strong></p>\n\n<p>Send and receive packets in a more convenient way:\n<ul>\n    <li>Send packets anytime without needing to go to a post office</li>\n    <li>Receive packets and pick them anytime you want</li>\n</ul>\n</p>\n\n<p><strong>Roles:</strong>\n<ul>\n    <li>Configuration of the main operating system</li>\n    <li>Configuration of the boot/fallback operating system</li>\n    <li>Planning and development of the remote update system</li>\n</ul>\n</p>\n\n<p><strong>Technologies Used:</strong>\n<ul>\n<li>Linux (TinyCore, Debian), Python, Shell Script</li>\n</ul>\n</p>','2016-02-06',''),(9,'Clinidata','Spring/GWT/Hibernate','<p><strong>Full featured healtcare solution</strong></p>\n<p>Clinidata it\'s an eHealth web application suited for clinical and non-clical organizations.</p>\n\n<p>The application is composed of several modules like:</p>\n<ul>\n	<li>Clinical pathology</li>\n	<li>Paperless microbiology</li>\n	<li>Anatomic pathology</li>\n	<li>Immunohematology</li>\n	<li>Electronic requisition</li>\n	<li>Epidemiological survelillance</li>\n	<li>Quality Control</li>\n	<li>Blood bank</li>\n</ul>\n	\n\n<p><strong>Roles:</strong> Developer</p>\n<p><strong>Technologies Used:</strong> Java, GWT, Hibernate, Spring, Liquibase, Oracle, Postgres, Mirth</p>','2017-06-05',NULL),(11,'MyAXA A@W','Spring/Angular/Mongo','<p><strong>MyAXA Accident@Work</strong></p>\n<p>Allows the victim of a work accident to keep track of their cases and reimburses</p>\n\n<p>Main features:</p>\n<ul>\n	<li>Upload and download documents</li>\n	<li>Upload and download expenses</li>\n	<li>Check the status of reimbursments and salary compensations</li>\n	<li>Responsive, adapts to mobile and desktop use</li>\n	<li>Communicates to stakeholders via e-mail and postal letters</li>\n</ul>\n\n<p><strong>Roles:</strong> Developer</p>\n<p><strong>Technologies Used:</strong> Java, Angular, MongoDB, Spring</p>','2020-01-15','https://www.axa.be/myaxaaccidentatwork/');
/*!40000 ALTER TABLE `portfolio_projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolio_projects_categories`
--

DROP TABLE IF EXISTS `portfolio_projects_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portfolio_projects_categories` (
  `id_category` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  PRIMARY KEY (`id_category`,`id_project`),
  KEY `project_idx` (`id_project`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Relation Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolio_projects_categories`
--

LOCK TABLES `portfolio_projects_categories` WRITE;
/*!40000 ALTER TABLE `portfolio_projects_categories` DISABLE KEYS */;
INSERT INTO `portfolio_projects_categories` VALUES (1,1),(1,2),(1,3),(1,5),(1,7),(1,9),(1,11),(2,4),(2,6),(2,8);
/*!40000 ALTER TABLE `portfolio_projects_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skills_main`
--

DROP TABLE IF EXISTS `skills_main`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skills_main` (
  `id_skill` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `small` varchar(45) DEFAULT NULL,
  `description` varchar(200) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '50',
  PRIMARY KEY (`id_skill`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='The main Skills\n';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skills_main`
--

LOCK TABLES `skills_main` WRITE;
/*!40000 ALTER TABLE `skills_main` DISABLE KEYS */;
INSERT INTO `skills_main` VALUES (1,'Typescript','','I started learning TypeScript and Angular in my spare time, now I deal with it on a daily basis at my job.',78),(2,'PHP','','I started doing web development with PHP back in 2014, nowadays I don\'t code in PHP that often.',60),(3,'Java','','I used Java everyday at work since 2017 and I\'m loving it. I work mostly with Spring Framework (Data, Security, Batch, Cloud) and Hibernate.',92);
/*!40000 ALTER TABLE `skills_main` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skills_other`
--

DROP TABLE IF EXISTS `skills_other`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skills_other` (
  `id_skills_other` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id_skills_other`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1 COMMENT='Less Relevant Skills';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skills_other`
--

LOCK TABLES `skills_other` WRITE;
/*!40000 ALTER TABLE `skills_other` DISABLE KEYS */;
INSERT INTO `skills_other` VALUES (1,'Linux'),(2,'.NET'),(3,'QT (PyQT)'),(4,'CherryPy'),(5,'CodeIgniter'),(6,'Javascript'),(7,'HTML5'),(8,'MVC'),(9,'Git'),(18,'Jenkins'),(11,'JSP'),(12,'REST'),(13,'Android'),(14,'OOP'),(17,'Hibernate'),(16,'PL/SQL'),(19,'Liquibase'),(20,'GWT'),(21,'Angular 2+'),(22,'Spring Framework'),(23,'Spring Cloud'),(24,'Python');
/*!40000 ALTER TABLE `skills_other` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testimonials` (
  `id_testimonial` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  `author_description` varchar(65) DEFAULT NULL,
  PRIMARY KEY (`id_testimonial`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testimonials`
--

LOCK TABLES `testimonials` WRITE;
/*!40000 ALTER TABLE `testimonials` DISABLE KEYS */;
/*!40000 ALTER TABLE `testimonials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `uid` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `username` varchar(20) NOT NULL,
  `email` varchar(64) NOT NULL,
  `regdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastlogin` datetime DEFAULT NULL,
  `hash` varchar(100) NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Bruno Jesus','brunojesus','bruno.fl.jesus@gmail.com','2016-11-30 11:29:15',NULL,'12345');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `values`
--

DROP TABLE IF EXISTS `values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `values` (
  `id_value` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `value` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id_value`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 COMMENT='Values for the header';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `values`
--

LOCK TABLES `values` WRITE;
/*!40000 ALTER TABLE `values` DISABLE KEYS */;
INSERT INTO `values` VALUES (1,'social_linkedin','https://www.linkedin.com/in/bruno-jesus-0ab943a9'),(2,'social_github-alt','https://gitlab.com/strang3quark'),(3,'social_skype','skype:brunojesuspt?chat'),(6,'contact_email','bruno.fl.jesus@gmail.com'),(7,'contact_phone','+351932438229'),(8,'getintouch_text','I\'m not actively looking for work, but if you want me to do some work for you, do not hesitate to contact me.\nIn my spare time I am developing the <a href=\"https://pricetracker.pt\" target=\"_blank\">PriceTracker</a>.'),(9,'getintouch_topics','App development in .NET, Python (PyQT) and Java;\nFrontend development with Angular.\nBackend development in Java/Python/PHP;\nWorking with Linux desktop, server and embedded systems;\nSome crazy project you might have in mind (I like a good challenge)'),(10,'about_text','<p>I\'m a 26 year old developer from Portugal currently working for Volkswagen Group Services @ MAN Digital Hub.</p>\n'),(11,'about_name','Bruno Jesus'),(12,'about_role','Developer');
/*!40000 ALTER TABLE `values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `view_post`
--

DROP TABLE IF EXISTS `view_post`;
/*!50001 DROP VIEW IF EXISTS `view_post`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_post` AS SELECT 
 1 AS `id`,
 1 AS `title`,
 1 AS `author_id`,
 1 AS `author_username`,
 1 AS `author_name`,
 1 AS `datetime`,
 1 AS `content`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_post_categories`
--

DROP TABLE IF EXISTS `view_post_categories`;
/*!50001 DROP VIEW IF EXISTS `view_post_categories`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_post_categories` AS SELECT 
 1 AS `post_id`,
 1 AS `category_id`,
 1 AS `post_title`,
 1 AS `category_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `work_experience`
--

DROP TABLE IF EXISTS `work_experience`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `work_experience` (
  `id_work` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(45) DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `time` varchar(30) DEFAULT NULL,
  `location` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id_work`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `work_experience`
--

LOCK TABLES `work_experience` WRITE;
/*!40000 ALTER TABLE `work_experience` DISABLE KEYS */;
INSERT INTO `work_experience` VALUES (1,'Wolfd','Developer','<p>Most of my time in this company was occupied by the development of Rediskey which turned out to be my first big project.</p>','2012 - 2013','Peniche'),(2,'ConnectedTI','Developer','<p>This was actually an Intership for my Especialization Course, I already had working experience so they put me in charge of a CRM for a Call Center.</p>','2014','Torres Vedras'),(3,'Redislogar','Freelancer Developer','<p>I was hired to configure the OS, system security and to develop an update system from scratch for their HomePaq machines.<p>','2016/02 and 2016/04','Cascais'),(4,'Maxdata','Developer','Development of Clinidata Healtcare Software','2017/06 - 2019/06','Carregado'),(5,'Alter Solutions (AXA Belgium)','Developer','Working on Alter Solutions as an Axa Belgium external developer at MyAXA Accident@Work','2019/06','Lisbon / Brussels'),(11,'Volkswagen Digital Solutions','Software Engineer','Working at Wolkswagen Digital Solutions - MAN Digital Hub','2020/03','Lisbon');
/*!40000 ALTER TABLE `work_experience` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'heroku_e3a47fd0ff1008a'
--

--
-- Final view structure for view `view_post`
--

/*!50001 DROP VIEW IF EXISTS `view_post`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`ba9e24a0985bea`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `view_post` AS select `blog_posts`.`id` AS `id`,`blog_posts`.`title` AS `title`,`blog_posts`.`author` AS `author_id`,`users`.`username` AS `author_username`,`users`.`name` AS `author_name`,`blog_posts`.`datetime` AS `datetime`,`blog_posts`.`content` AS `content` from (`blog_posts` join `users`) where (`users`.`uid` = `blog_posts`.`author`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_post_categories`
--

/*!50001 DROP VIEW IF EXISTS `view_post_categories`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`ba9e24a0985bea`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `view_post_categories` AS select `blog_posts`.`id` AS `post_id`,`blog_categories`.`id` AS `category_id`,`blog_posts`.`title` AS `post_title`,`blog_categories`.`name` AS `category_name` from ((`blog_posts_categories` join `blog_posts`) join `blog_categories`) where ((`blog_posts`.`id` = `blog_posts_categories`.`post`) and (`blog_categories`.`id` = `blog_posts_categories`.`category`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-19 15:22:02
