<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?=$about['name'].' - '.$about['page'];?></title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Bruno Jesus Web Portfolio">
    <meta name="author" content="<?=$about['name'];?>">
    <link rel="shortcut icon" href="favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,400italic,300italic,300,500italic,700,700italic,900,900italic" rel="stylesheet" type="text/css">
    <!-- Global CSS -->
	<link rel="stylesheet" href="<?=base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Plugins CSS -->
    <!--<link rel="stylesheet" href="<?=base_url();?>assets/plugins/font-awesome/css/font-awesome.css">-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/styles-gray.css">
    <!-- Full Screen Modals -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/modal-fullscreen.css">

	<?php
	//If we are in a blog page load the blog css
	if ($about['page'] == "Blog"): ?>
	<link rel="stylesheet" href="<?=base_url();?>assets/css/blog.css">
	<?php endif; ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
