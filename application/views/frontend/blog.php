<body>
    <header class="header">
        <div class="page-nav-space-holder hidden-xs"> <!--Not for XS-->
            <div id="page-nav-wrapper" class="page-nav-wrapper">
                <div class="container">
					<ul id="site-nav" class="nav page-nav list-inline">
						<li><a href="<?=base_url();?>blog">All</a></li>
						<?php foreach ($categories as $category): ?>
							<li><a href="<?=base_url();?>blog/category/<?=$category['name'];?>"><?=$category['name'];?></a></li>
						<?php endforeach; ?>
					</ul>
                    <ul id="page-nav" class="nav page-nav list-inline pull-right">
                        <li><a href="<?=base_url();?>frontend">Portfolio</a></li>
                    </ul><!--//page-nav-->
                </div>
            </div><!--//page-nav-wrapper-->
        </div>

		<div class="hidden-sm hidden-md hidden-lg">
			<nav class="navbar navbar-custom">
			  <div class="container-fluid">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#small-nav" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      <a class="navbar-brand" href="#">Bruno Jesus - Blog</a>
			    </div>
			
			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="small-nav">
			      <ul class="nav navbar-nav">
					<li><a href="<?=base_url();?>blog">All</a></li>
					<?php foreach ($categories as $category): ?>
						<li><a href="<?=base_url();?>blog/category/<?=$category['name'];?>"><?=$category['name'];?></a></li>
					<?php endforeach; ?>
	            	<li role="separator" class="divider"></li>
                    <li><a href="<?=base_url();?>frontend">Portfolio</a></li>
			      </ul>
			    </div><!-- /.navbar-collapse -->
			  </div><!-- /.container-fluid -->
			</nav>
		</div> <!--//Compact NAV -->
    </header><!--//header-->

    <div class="wrapper container">
		<?php if ($posts === false): ?>
			<h1>Content not found</h1>	
		<?php else: ?>	
			<?php foreach ($posts as $post): ?>
	   			<section class="section">
					<h2 class="section-title"><?=$post['article']['title'];?></h2>
					<?php
						$date = explode(" ",$post['article']['datetime'])[0];
						$strCategories = "";
						foreach ($post['categories'] as $category){
							$strCategories .= '<a href="'.base_url().'blog/category/'.$category.'">'.$category.'</a>, ';
						}
						$strCategories = rtrim($strCategories,', ');

					?>
					<div class="post-info clearfix">
						<div class="pull-left">
							<?=$strCategories;?>
						</div>
						<div class="pull-right">
							<?=$date;?>
						</div>
					</div>
	   			    <div class="row">
						<div class="item col-xs-12 article">
							<!--TODO: article class to fix headers-->
							<?php 
								$content = $this->markdown->parse($post['article']['content']);
								//If it's a list of articles cut the text
								$content = ($mode == 'list')?cut_text_after($content):$content;?>
							<?=$content;?>
							

							<?php 
							//If it's a list of articles show the read-more button
							if ($mode == 'list'): ?>
								<hr>
								<div class="text-center readmore">
									<a href="<?=base_url()."blog/post/".$post['article']['id'];?>">Read More</a>
								</div>
							<?php endif; ?>
	   			        </div>
	   			    </div><!--//row-->
	   			</section><!--//section-->
 			<?php endforeach; //post list ?>
			<?php if ($mode == 'single'): ?>
				<section class="section">
					<h2 class="section-title">Comments</h2>
					<div class="row">
						<div class="item col-xs-12 article">
							<p>This section is still in development.</p>
							<p>If you want to share any thoughts drop me a line at <a href="mailto:bruno.fl.jesus@gmail.com">bruno.fl.jesus@gmail.com</a></p>
						</div>
					</div>
				</section>
			<?php endif; //comment box ?>
		<?php endif; //posts === false ?>
    </div><!--//wrapper-->

    <footer class="footer text-center">
        <div class="container">
            <small class="copyright">Copyright @ <a href="http://brunojesus.tk/" target="_blank">Bruno Jesus</a></small>
        </div><!--//container-->
    </footer>


    <!-- Javascript -->
    <script type="text/javascript" src="<?=base_url()?>assets/plugins/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/plugins/back-to-top.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/plugins/jquery-scrollTo/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/plugins/easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/plugins/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/plugins/isotope.pkgd.min.js"></script>

    <!-- custom js -->
    <script type="text/javascript" src="<?=base_url()?>assets/js/main.js"></script>

</body>
</html>
