<body>
    <header class="header">
        <div class="top-bar container-fluid">
            <div class="actions">
                <!--<a class="btn" href="blog"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Blog</a>-->
                <a class="btn hidden-xs" href="mailto:<?=$contact['email'];?>"><i class="fa fa-paper-plane" aria-hidden="true"></i> Hire Me</a>
                <a class="btn" href="assets/resume/resume.pdf"><i class="fa fa-download" aria-hidden="true"></i> Download My Resume</a>
            </div><!--//actions-->
            <ul class="social list-inline">
                <?php foreach ($social as $key => $value): ?>
                  <li><a href="<?=$value;?>"><i class="fa fa-<?=$key;?>" aria-hidden="true"></i></a></li>
                <?php endforeach; ?>
            </ul><!--//social-->
        </div><!--//top-bar-->

        <div class="intro">
            <div class="container text-center">
                <img class="profile-image" src="<?=$base_url;?>assets/images/profile-image.png" alt="">
                <h1 class="name"><?=$about['name'];?></h1>
                <div class="title"><?=$about['role'];?></div>
                <div class="profile">
                    <?=$about['text'];?>
                </div><!--//profile-->
            </div><!--//container-->
        </div><!--//intro-->

        <div class="contact-info">
            <div class="container text-center">
                <ul class="list-inline">
                    <li class="email"><i class="fa fa-envelope"></i><a href="mailto:<?=$contact['email'];?>"><?=$contact['email'];?></a></li>
                    <li><i class="fa fa-phone"></i> <a href="tel:<?=$contact['phone'];?>"><?=$contact['phone'];?></a></li>
                    <li class="website"><i class="fa fa-globe"></i><a href="<?=$base_url;?>" target="_blank"><?="brunojesus.pt";?></a></li>
                </ul>
            </div><!--//container-->
        </div><!--//contact-info-->

        <div class="page-nav-space-holder hidden-xs">
            <div id="page-nav-wrapper" class="page-nav-wrapper text-center">
                <div class="container">
                    <ul id="page-nav" class="nav page-nav list-inline">
			<!--<li><a href="<?=base_url();?>blog">Blog</a></li>-->
                        <li><a class="scrollto" href="#experiences-section">Experiences</a></li>
                        <li><a class="scrollto" href="#education-section">Education</a></li>
                        <li><a class="scrollto" href="#skills-section">Skills</a></li>
                        <!-- <li><a class="scrollto" href="#testimonials-section">Testimonials</a></li> -->
                        <li><a class="scrollto" href="#portfolio-section">Portfolio</a></li>
                        <li><a class="scrollto" href="#contact-section">Contact</a></li>
                    </ul><!--//page-nav-->
                </div>
            </div><!--//page-nav-wrapper-->
        </div>

    </header><!--//header-->

    <div class="wrapper container">
        <section id="experiences-section" class="experiences-section section">
          <h2 class="section-title">Work Experiences</h2>
          <div class="timeline">
            <?php foreach ($work as $job): ?>
              <div class="item">
                <div class="work-place">
                  <h3 class="place"><?=$job['company'];?></h3>
                  <div class="location"><i class="fa fa-map-marker" aria-hidden="true"></i><?=$job['location'];?></div>
                </div><!--//work-place-->
                <div class="job-meta">
                  <div class="title"><?=$job['role'];?></div>
                  <div class="time"><?=$job['time'];?></div>
                </div><!--//job-meta-->
                <div class="job-desc">
                  <?=$job['description'];?>
                </div><!--//job-desc-->
              </div><!--//item-->
            <?php endforeach; ?>
          </div><!--//timeline-->
        </section><!--//experiences-section-->

        <section id="education-section" class="education-section section">
            <h2 class="section-title">Education</h2>
            <div class="row">
              <?php foreach ($education as $edu): ?>
                <div class="item col-xs-12 col-sm-4">
                  <div class="item-inner">
                    <h3 class="degree"><?=$edu['course'];?></h3>
                    <div class="education-body">
                      <?=$edu['institution'];?>
                    </div>
                    <div class="time"><?=$edu['start']." - ".$edu['end'];?></div>
                    <div class="desc">
                      <?=$edu['description'];?>
                    </div>
                  </div>
                </div>
              <?php endforeach; ?>
            </div><!--//row-->
        </section><!--//section-->


        <section id="skills-section" class="skills-section section text-center">
            <h2 class="section-title">Professional Skills</h2>
            <div class="top-skills">
                <h3 class="subtitle">Top Skills</h3>
                <div class="row">
                  <?php foreach ($skills_main as $skill): ?>
                    <div class="item col-xs-12 col-sm-4">
                      <div class="item-inner">
                        <img src="<?=$base_url;?>assets/images/skill/<?=strtolower($skill['name'])?>.svg" alt="">

                        <!-- <div class="chart-easy-pie text-center">
                          <div class="chart" data-percent="<?=$skill['level'];?>">
                            <span><?=$skill['level'];?></span>%
                          </div>
                        </div>//chart -->
                        <h4 class="skill-name"><?=$skill['name'];?></h4>
                        <div class="level"><?=$skill['small'];?></div>
                        <div class="desc">
                          <?=$skill['description'];?>
                        </div>
                      </div><!--//item-inner-->
                    </div><!--//item-->
                  <?php endforeach; ?>
                </div><!--//row-->
            </div><!--//top-skills-->

            <div class="other-skills">
                <h3 class="subtitle">Other Skills</h3>
                <div class="misc-skills">
                  <?php
                  $i = 1; //current skill
                  foreach ($skills_other as $skill): ?>
                    <span class="skill-tag"><?=$skill['name'];?></span>
                    <?=(($i % 5) == 0)?"<br />":""; $i++;?>
                  <?php endforeach; ?>
                </div>
            </div><!--//other-skills-->

        </section><!--//skills-section-->

        <section id="testimonials-section" class="testimonials-section section" style="display: none">
          <h2 class="section-title">Testimonials</h2>

          <div id="testimonials-carousel" class="testimonials-carousel carousel slide" data-interval="8000">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <?php for ($i=0; $i<count($testimonials); $i++): ?>
                <li data-target="#testimonials-carousel" data-slide-to="<?=$i;?>"
                  <?=($i==0) ? 'class="active"' : "" ;?>></li>
                <?php endfor; ?>
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner">
                <?php $first=true; ?>
                <?php foreach ($testimonials as $testimonial): ?>
                  <div class="item <?=($first)?'active':''; $first=false;?>">
                    <blockquote class="quote">
                      <i class="fa fa-quote-left"></i>
                      <p><?=$testimonial['text'];?></p>
                    </blockquote>
                    <div class="source">
                      <div class="name"><?=$testimonial['author'];?></div>
                      <div class="position"><?=$testimonial['author_description'];?></div>
                    </div><!--//source-->
                  </div><!--//item-->
                <?php endforeach; ?>
              </div><!--//carousel-inner-->
            </div><!--//testimonials-carousel-->
        </section><!--//section-->

        <section id="portfolio-section" class="portfolio-section section">
          <h2 class="section-title">Portfolio</h2>
          <ul id="filters" class="filters clearfix">
            <li class="type active" data-filter="*">All</li>
            <?php foreach ($portfolio_categories as $cat): ?>
            <li class="type" data-filter=".<?=strtolower($cat['name']);?>"><?=$cat['name'];?></li>
            <?php endforeach; ?>
          </ul><!--//filters-->
          <div class="items-wrapper isotope row">
            <?php foreach ($portfolio_items as $item): ?>
              <div class="item <?=strtolower($item['category']);?> col-md-3 col-xs-6">
                <div class="item-inner">
                  <figure class="figure">
                    <img class="img-responsive" src="<?=$base_url;?>assets/images/portfolio/<?=$item['id_project'];?>/thumb.png" alt="" />
                  </figure>
                  <div class="content text-left">
                    <h3 class="sub-title"><a href="#"><?=$item['name'];?></a></h3>
                    <div class="meta"><?=$item['description_short'];?></div>
                    <div class="action"><a href="#">View More</a></div>
                  </div><!--//content-->
                  <!--<a class="link-mask" data-toggle="modal" data-target="#modal-fullscreen"></a>-->
                  <a class="link-mask" id="project_<?=$item['id_project'];?>"></a>
                </div><!--//item-inner-->
              </div><!--//item-->
            <?php endforeach; ?>
          </div><!--//item-wrapper-->
        </section><!--//section-->

        <section id="contact-section" class="contact-section section">
            <h2 class="section-title">Get in Touch</h2>
            <div class="intro">
                <img class="profile-image" src="<?=$base_url;?>assets/images/profile-image.png" alt="">
                <div class="dialog">
                    <p><?=$getintouch['text'];?></p>
                    <p><strong>I can help with the following:</strong></p>
                    <?php $topics=explode(';',$getintouch['topics']);?>
                    <ul class="list-unstyled service-list">
                      <?php foreach ($topics as $topic): ?>
                        <li><i class="fa fa-check" aria-hidden="true"></i> <?=$topic;?></li>
                      <?php endforeach; ?>
                    </ul>
                    <p>Drop me a line at <a href="mailto:<?=$contact['email'];?>"><?=$contact['email'];?></a> or call me at <a href="tel:<?=$contact['phone'];?>"><?=$contact['phone'];?></a></p>
                    <ul class="social list-inline">
                      <?php foreach ($social as $key => $value): ?>
                        <li><a href="<?=$value;?>"><i class="fa fa-<?=$key;?>" aria-hidden="true"></i></a></li>
                      <?php endforeach; ?>
                    </ul><!--//social-->
                </div><!--//diaplog-->
            </div><!--//intro-->

        </section><!--//section-->

    </div><!--//wrapper-->

    <footer class="footer text-center">
        <div class="container">
            <small class="copyright">Copyright @ <a href="http://brunojesus.tk/" target="_blank">Bruno Jesus</a></small>
        </div><!--//container-->
    </footer>


    <!--Portfolio item fullscreen-->
    <div class="modal modal-fullscreen fade" id="modal-project" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="modal-project-title"></h4>
          </div>
          <div class="modal-body" id="modal-project-body">
            <div class="row">
            <div class="col-sm-4 pull-right">
              <img id="modal-body-image" src="" alt="">
            </div>
            <div class="col-sm-8">
              <div id="modal-body-text"></div>
            </div>
            </div>
					</div> <!--//modal-body-->
        </div><!--//modal-content-->
        <div class="modal-footer">
          <a href="#" id="modal-link" role="button" class="btn btn-default">View More</a>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div><!--//modal-dialog-->
    </div><!--//modal-fullscreen-->
  <!--//Portfolio item fullscreen-->

    <!-- Javascript -->
    <script type="text/javascript" src="<?=$base_url;?>assets/plugins/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="<?=$base_url;?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=$base_url;?>assets/plugins/back-to-top.js"></script>
    <script type="text/javascript" src="<?=$base_url;?>assets/plugins/jquery-scrollTo/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="<?=$base_url;?>assets/plugins/easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
    <script type="text/javascript" src="<?=$base_url;?>assets/plugins/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="<?=$base_url;?>assets/plugins/isotope.pkgd.min.js"></script>

    <!-- custom js -->
    <script type="text/javascript" src="<?=$base_url;?>assets/js/main.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-86980858-1', 'auto');
  ga('send', 'pageview');
</script>

</body>
</html>
