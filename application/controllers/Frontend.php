<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->model('mportfolio');
	}

	/**
	* Website startpage, this will be a one page template
	* that means that this is the only public function of the frontend
	*/
	public function index()
	{
		$frontendData = array();
		$frontendData['base_url'] = base_url();
		$sections = array('about','social','contact','getintouch');
		foreach ($sections as $section){
			$frontendData[$section] = $this->mportfolio->get_values_section($section);
		}

		$frontendData['work'] = $this->mportfolio->get_jobs();
		$frontendData['education'] = $this->mportfolio->get_education();
		$frontendData['skills_main'] = $this->mportfolio->get_skills_main();
		$frontendData['skills_other'] = $this->mportfolio->get_skills_other();
		$frontendData['testimonials'] = $this->mportfolio->get_testimonials();
		$frontendData['portfolio_items'] = $this->mportfolio->get_portfolio_short();
		$frontendData['portfolio_categories'] = $this->mportfolio->get_portfolio_categories();

		$frontendData['about']['page'] = "Resume";
		$this->load->view('frontend/head', array("about" => $frontendData['about']));
		$this->load->view("frontend/page", $frontendData);
	}

	public function project($id){
		$item = $this->mportfolio->get_portfolio_item($id);
		$item['image'] = base_url()."assets/images/portfolio/$id/image.png";
		echo json_encode($item,JSON_UNESCAPED_SLASHES);
	}
}
