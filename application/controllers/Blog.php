<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->helper('my_intellicut');
		//$this->load->model('mportfolio');
		$this->load->model('mblog');
		$this->load->library('markdown');
	}

	public function index(){
		//$data['about'] = $this->mportfolio->get_values_section('about');
		$data = array('about'=>array('page'=>'Blog', 'name'=>'All'));
		$data['posts'] = $this->mblog->get_posts();
		$data['mode'] = 'list';
		$data['categories'] = $this->mblog->get_categories();
		$this->load->view('frontend/head.php',$data);
		$this->load->view('frontend/blog.php');
	}

	public function category($categoryName='all'){
		$data = array('about'=>array('page'=>'Blog', 'name'=>$categoryName));
		$data['posts'] = $this->mblog->get_posts($categoryName);
		$data['mode'] = 'list';
		$data['categories'] = $this->mblog->get_categories();
		$this->load->view('frontend/head.php',$data);
		$this->load->view('frontend/blog.php');
	}

	public function post($postId){
		$post = $this->mblog->get_post($postId);
		$data['about'] = array('page'=>'Blog',	'name'=>$post['article']['title']);
		if ($post === false){
			$data['posts'] = false;
			$data['about']['name'] = "Not Found";
		}else{
			$data['posts'] = array();
			$data['posts'][] = $post;
		}
		$data['mode'] = 'single';
		$data['categories'] = $this->mblog->get_categories();

		$this->load->view('frontend/head.php',$data);
		$this->load->view('frontend/blog.php', $data);
	}
}
