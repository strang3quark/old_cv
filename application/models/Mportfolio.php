<?php
class MPortfolio extends CI_Model {

  public function __construct()
  {
    $this->load->database();
  }

  public function get_value($name){
    $query = $this->db->select('value')->from('values')
      ->where('name', $name)
    ->get();

    return $query->row()->value;
  }

  public function get_values_section($section){
    $query = $this->db->select('name,value')->from('values')
      ->like('name', $section.'_','after')
    ->get();

    $rows = $query->result_array();

    $result = array();
    foreach ($rows as $row){
      #remove $section from key
      $key = substr($row['name'],strlen($section.'_'));
      $value = $row['value'];
      $result[$key] = $value;
    }

    return $result;
  }

  public function get_skills_main(){
      $query = $this->db->select('name,small,description,level')
        ->from('skills_main')->order_by('level','DESC')->get();
      return $query->result_array();
  }

  public function get_skills_other(){
    $query = $this->db->select('name')->from('skills_other')->get();
    return $query->result_array();
  }

  public function get_jobs(){
    $query = $this->db->select('company,role,description,time,location')
      ->from('work_experience')->order_by('id_work','DESC')->get();
    return $query->result_array();
  }

  public function get_education(){
    $query = $this->db->select('course,institution,description,start,end')
      ->from('education')->order_by('start','DESC')->get();
    return $query->result_array();
  }

  public function get_testimonials(){
    $query = $this->db->select('text,author,author_description')
      ->from('testimonials')->order_by('id_testimonial','DESC')->get();
    return $query->result_array();
  }

  public function get_portfolio_short(){
    $query = $this->db->select('pp.id_project, pp.name ,pp.description_short, ca.name as category')
      ->from('portfolio_categories as ca')
      ->join('portfolio_projects_categories ppc','ca.id_pcategory = ppc.id_category')
      ->join('portfolio_projects as pp','ppc.id_project = pp.id_project')
      ->order_by('release','DESC')->get();
    return $query->result_array();
  }

  public function get_portfolio_categories(){
    $query = $this->db->select('name')->from('portfolio_categories')->get();
    return $query->result_array();
  }

  public function get_portfolio_item($id_project){
      $query = $this->db->select('pp.id_project, pp.name ,pp.description_long, pp.link, ca.name as category')
      ->from('portfolio_categories as ca')
      ->join('portfolio_projects_categories ppc','ca.id_pcategory = ppc.id_category')
      ->join('portfolio_projects as pp','ppc.id_project = pp.id_project')
      ->where('pp.id_project',$id_project)->get();
    return $query->row_array();
  }
}
