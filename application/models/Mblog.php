<?php
class MBlog extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	/**
	 * Get Posts
	 *
	 * Return a list of posts in the database
	 * It can return posts in a specific category and has page support
	 * TODO: Page support and category filter
	 *
	 * @param	string	category		The category name to filter
	 * @param	int		page			The page number
	 * @param	int		postsPerPage	The number of posts per text
	 * @return	array	An numbered array containing 2 arrays @ret[index]=>(article[a=>a,b=>b], categories[index])
	 */
	public function get_posts($category = 'all', $page = 1, $postsPerPage=20){
		if ($category == 'all'){
			$query = $this->db->select('*')->from('view_post');
		}else{
			//get the id of this category
			$catQuery = $this->db->select('id')->from('blog_categories')
				->where('name',$category)->get();;
			$catQueryResult = $catQuery->result_array();
			if (count($catQueryResult) == 0){
				return false;
			}
			$categoryId = $catQueryResult[0]['id'];

			$query = $this->db->select('*')->from('view_post, blog_posts_categories')
				->where('view_post.id = blog_posts_categories.post')
				->where('blog_posts_categories.category', $categoryId);
		}
		$query = $query->order_by('view_post.datetime','desc')->get();

		$articles = $query->result_array();
		$posts = array();
		
		//put in a article[],categories[] array
		foreach ($articles as $article){
			$post = array();
			$post['article'] = $article;
			$post['categories'] = $this->get_post_categories($article['id']);
			$posts[] = $post; //append post to posts array
		}

		return $posts;
	}

	/**
	 * Get Post
	 *
	 * Returns an array with the specific post and it's categories
	 *
	 * @param	int		id	The id of the post to retrieve
	 * @return	array 	An associative array containing 2 arrays article[],categories[]
	 */
	public function get_post($id){
		$queryPost = $this->db->select('id,title,author_id, author_name,datetime,content')
			->from('view_post')
			->where('id',$id)->get();

		$ret = array();
		$queryResult = $queryPost->result_array();
		if (count($queryResult) == 0){
			return false;
		}
		$ret['article'] = $queryResult[0];
		$ret['categories'] = $this->get_post_categories($id);
		return $ret;
	}

	/**
	 * Get Post Categories
	 *
	 * Returns an numbered array containing all category names of a specific post
	 *
	 * @param	int		id	The id of the post
	 * @return	array 	The numbered array containing the category names as String
	 */
	public function get_post_categories($id){
		$queryCategories = $this->db->select('category_name')
			->from('view_post_categories')
			->where('post_id',$id)->get();
		
		$associativeArray = $queryCategories->result_array();

		//convert from associativeArray to an numbered array
		$list = array();
		foreach ($associativeArray as $category){
			$list[] = $category['category_name'];
		}

		return $list;
	}

	/**
	 * Get All Categories
	 *
	 * Returns an associative array containing all categories
	 *
	 * @return array array[index] => (id, name)
	 */
	public function get_categories(){
		$query = $this->db->select('id,name')->from('blog_categories')->get();
		return $query->result_array();
	}
}
