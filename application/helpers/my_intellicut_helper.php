<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Cut Text helper
 *
 * @author Bruno Jesus (bruno.fl.jesus@gmail.com)
 * @license MIT License
 */

if ( ! function_exists('cut_text_after')){

	/**
	 * Cut text until specific simbol appears after specific number of chars
	 *
	 * @param	string	text		The string to cut
	 * @param	int		size		The minimum amount of text
	 * @param	string	stop_char	Will cut until this char is found after size
	 */

	function cut_text_after($text = '', $size = 650, $stop_char = '>'){
		$retString = '';

		//if the size is bigger than the text return the text unchanged
		if ($size >= strlen($text)){
			return $text;
		}

		$retString = substr($text,0,$size);

		//if the retString already finishes in $stop_char
		if (substr($retString, -1) == $stop_char){
			return $retString;
		}
		

		$secondHalf = substr($text,$size);
		$secondSplit = explode($stop_char, $secondHalf);
		//if there is no $stop_char in the second half
		if (count($secondSplit) == 0){
			return $retString;
		}else{
			$retString .= $secondSplit[0];
		}

		return $retString;
	}
}
