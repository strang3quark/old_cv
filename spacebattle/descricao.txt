--Background--
 - Semelhante ao ficheiro Background dado pelo professor nos exemplos.
 - O background está sempre em movimento.

--Bala--
 - Bala disparada pela banheira
 - Semelhante ao ficheiro Bala dado pelo professor nos exemplos

--BalaE--
 - Bala disparada pelo inimigo
 - Semelhante ao ficheiro Bala
 - a propriedade tamanho define o tamanho da bala, inimigos menores terão balas mais pequenas.

--Banheira--
 - Tem um estado para cada direção
 - Baseado no Tank.js fornecido pelo professor
 - Fazemos um scale porque a sprite é muito grande

--Inimigo--
 -Baseado na banheira
 -Tem um estado para bico aberto e fechado
 -O tamanho é gerado aleatoriamente pelo principal.js
 -Abre a boca em um intervalo especifico de tempo
 	-O intervalo é reduzido á medida que o nivel aumenta
	-Quando a boca abre o ficheiro principal cria a balaAbre a boca em um intervalo especifico de tempo
		-O intervalo é reduzido á medida que o nivel aumenta
		-Quando a boca abre o ficheiro principal cria a bala


Logica de jogo

O jogo tem 10 niveis, à medida que o nível aumenta a banheira e os inimigos ficam mais rápidos e disparam com mais frequencia, podiamos ter colocado mais niveis mas a dificuldade de passar o jogo seria impossivél.
O local de spawn e tamanho dos inimigos são aleatorios.
Sempre que a banheira colidir com um inimigo ou uma bala o jogo acaba, a não ser que esteja em GodMode (activado pela tecla G).

