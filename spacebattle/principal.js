// Animated tank.js

var canvas;
var drawingSurface;
var entities = [];
var teclas= new Array(255);
var oBackground;
var asBalasInimigos=[];
var asBalas=[];
var inimigos = [];
var debugMode=false;
var animationHandler;
var nivel = 10; //nivel actual, do 10 ao 1
var levelup = 10; //nr de inimigos que faltam para aumentar nivel
var score = 0; //numero de inimigos atingidos

var godMode = false;

var displayScore = undefined;
var displayLevel = undefined;

var GameStates={RUNNING:1,
				PAUSED:2,
				STOPED:3,
				LOADING:4,
				LOADED:5,
				OVER:6
			}
var GameSounds={
				FUNDO:{},
				TIRO:{},
				ATINGIDO:{},
				};

var gameState=undefined;

window.addEventListener("load",init, false);

function init(){
	canvas = document.querySelector("#eCanvas");
	drawingSurface = canvas.getContext("2d");

	cpCanvas = document.querySelector("#cpCanvas");
	cpDrawingSurface = cpCanvas.getContext("2d");

	gameState=GameStates.LOADING;

	var spBackground= new SpriteSheet();
	spBackground.load("assets//background.png", "assets//background.json",loaded);

	var spBanheira = new SpriteSheet();
	spBanheira.load("assets//banheira.png", "assets//banheira.json",loaded);

	var spInimigo = new SpriteSheet();
	spInimigo.load("assets//flappy.png", "assets//flappy.json",loaded);


	//sons
	gSoundManager.loadAsync("assets/sounds/background.mp3", function(so){GameSounds.FUNDO.normal=so;loaded("assets/sounds/background.mp3")});
	gSoundManager.loadAsync("assets/sounds/background_god.mp3", function(so){GameSounds.FUNDO.god=so;loaded("assets/sounds/background_god.mp3")});
	gSoundManager.loadAsync("assets/sounds/Bala.mp3", function(so){GameSounds.TIRO.banheira=so;loaded("assets/sounds/Bala.mp3")});
	gSoundManager.loadAsync("assets/sounds/Tlum.mp3", function(so){GameSounds.ATINGIDO.flappy=so;loaded("assets/sounds/Tlum.mp3")});
	gSoundManager.loadAsync("assets/sounds/Pato.mp3", function(so){GameSounds.ATINGIDO.banheira=so;loaded("assets/sounds/Pato.mp3")});
}

function loaded(assetName){
 if(Object.keys(gSpriteSheets).length<3) return;
 console.log(Object.keys(gSpriteSheets).length + " entidades carregadas");

  gameState=GameStates.LOADED;
  oBackground= new Background(gSpriteSheets['assets//background.png'], 0, 0);
  
  canvas.width=window.innerWidth;
  canvas.height=window.innerHeight;
  cpCanvas.width=window.innerWidth;
  cpCanvas.height=window.innerHeight;


  banheira= new Banheira(gSpriteSheets['assets//banheira.png'], 20, 20);

  entities.push(oBackground);
  entities.push(banheira);

  GameSounds.FUNDO.normal.play(true,0.5);
  displayScore = new DisplayScore(5,5,250,50,cpDrawingSurface,"Score",'0',"red","black","white");
  displayLevel = new DisplayScore(canvas.width-250,5,250,50,cpDrawingSurface,"Level",'1',"yellow","black","white");
  //Update the sprite as soon as the image has been loaded

  gameState=GameStates.RUNNING;
  window.addEventListener("keydown",keyDownHandler,false);
  window.addEventListener("keyup",keyUpHandler,false);

  update();
  gerarInimigo();
}

function randomIntFromInterval(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}

function gerarInimigo(){
	posY = randomIntFromInterval(100,canvas.height-170);
	tamanho = randomIntFromInterval(3,10) / 10;
	umInimigo = new Inimigo(gSpriteSheets['assets//flappy.png'], canvas.width+200, posY, 12-nivel, tamanho);
	entities.push(umInimigo);
	inimigos.push(umInimigo);
	setTimeout(gerarInimigo, nivel*400); //voltar a chamar a funcao, o intervalo reduz consoante o nivel
}

function alternarGodMode(){
	  godMode = !godMode;
	  console.log("GodMode="+godMode);
	  if (godMode){
		GameSounds.FUNDO.normal.stop();
		GameSounds.FUNDO.god.play(true,0.5);
	  }else{
		GameSounds.FUNDO.god.stop();
		GameSounds.FUNDO.normal.play(true,0.5);
	  }
}
function keyDownHandler(e){
	var codTecla=e.keyCode;
	teclas[codTecla]=true;
}

function keyUpHandler(e){
	var codTecla=e.keyCode;
	teclas[codTecla]=false;
	switch(codTecla){
		case  keyboard.g		  :  alternarGodMode(); break;
		case  keyboard.SPACE	  :  banheira.podeDisparar=true;  break;
	}
}

function scoreUp(){
	score++;
	levelup--;
	
	if (levelup == 0){
		levelup = 10;
		(nivel>1)?nivel--:nivel;
		displayLevel.setValue(11-nivel);
		oBackground.vx=(11-nivel);
	}

	if (debugMode){
	 console.log("SCORE: "+score);
	 console.log("NIVEL: "+nivel);
	 console.log("FALTAM: "+levelup);
	}
	
	displayScore.setValue(score);
}

function gameOver(){
	banheira.fall();
	GameSounds.ATINGIDO.banheira.play(false, 1);
	window.removeEventListener("keyup",keyUpHandler);
	window.removeEventListener("keydown",keyDownHandler);
	GameSounds.FUNDO.normal.stop();
}

function checkColisions(){
	for(var i=0; i<asBalas.length; i++){
		if(asBalas[i].right()<0||
		   asBalas[i].left()>canvas.width ||
  		   asBalas[i].bottom() < 0 ||
		   asBalas[i].top()> canvas.height){
			   asBalas[i].active=false;
		}
		
		//bala atinge inimigo
		for (var x=0; x < inimigos.length; x++){
			if(inimigos[x].hitTestRectangle(asBalas[i]) && inimigos[x].isColling != true){
				if (inimigos[x].isColliding == false){
					inimigos[x].isColliding=true;
					asBalas[i].isColliding=true;
					asBalas[i].active=false;
					inimigos[x].killed=true;
					GameSounds.ATINGIDO.flappy.play(false, 1);
					scoreUp();			
				}
			}
		}
	}
	
	for (var i=0; i<asBalasInimigos.length; i++){
		//bala saiu
		if(asBalasInimigos[i].right()<0||
		   asBalasInimigos[i].left()>canvas.width ||
  		   asBalasInimigos[i].bottom() < 0 ||
		   asBalasInimigos[i].top()> canvas.height){
			   asBalasInimigos[i].active=false;
		}
		
		//bala atinge banheira
		if (banheira.hitTestRectangle(asBalasInimigos[i])){
			asBalasInimigos[i].active=false;
			asBalasInimigos[i].isColliding=true;
			if (godMode !== true && banheira.isColliding == false){
				banheira.isColliding=true;
				gameOver();
			}
		}
		
		//bala atinge bala
		for(var x=0; x<asBalas.length; x++){
			if (asBalas[x].hitTestRectangle(asBalasInimigos[i])){
				asBalas[x].active = false;
				asBalasInimigos[i].active = false;
			}
		}
	}
	
	
	for (var i=0; i<inimigos.length; i++){
		if(inimigos[i].right()<0 || inimigos[i].bottom < 0){
			inimigos[i].active=false;
		}
		
		//inimigo bate contra banheira
		if (inimigos[i].hitTestRectangle(banheira) && inimigos[i].isColliding == false){
 			if (godMode !== true)
				gameOver();
			else{
				scoreUp();
				GameSounds.ATINGIDO.flappy.play(false, 1);
			}
			inimigos[i].isColliding=true;
			inimigos[i].killed=true;
		}
		
		//disparar inimigos
		if (inimigos[i].podeDisparar == true){
			inimigos[i].podeDisparar=false;
			var umaBala = new BalaE(gSpriteSheets['assets//flappy.png'], inimigos[i].x, inimigos[i].y+(inimigos[i].height/2), 16-nivel, inimigos[i].tamanho);
			asBalasInimigos.push(umaBala);
			entities.push(umaBala);
		}

	}

	//banheira saiu do canvas
	if (banheira.y > canvas.height){
		gameState = GameStates.OVER;
	}
}

function update(){
if(gameState==GameStates.RUNNING){
  if (teclas[keyboard.LEFT]){
	if (banheira.left() > -200){
		banheira.goLeft();
		banheira.x-=14-nivel;
	}
  }if(teclas[keyboard.RIGHT]){
	  if (banheira.right() < canvas.width){
		banheira.goRight();
		banheira.x+=14-nivel;
	  }

  }if(teclas[keyboard.UP]){
	  if (banheira.top() > -120) {
		banheira.goUp();
		banheira.y-=14-nivel;
	  }

  }if(teclas[keyboard.DOWN]){
	  if (banheira.bottom() < canvas.height){
		banheira.goDown();
		banheira.y+=14-nivel;
	  }
  }
  
  if ((teclas[keyboard.LEFT] != true && teclas[keyboard.RIGHT] != true && teclas[keyboard.UP] != true && teclas[keyboard.DOWN] != true)){
	 banheira.goRight();
  }


  if (teclas[keyboard.SPACE] != false && banheira.podeDisparar == true){
    banheira.podeDisparar=false;
    var umaBala = new Bala(gSpriteSheets['assets//banheira.png'], banheira.x+banheira.width-50,banheira.y+banheira.getHalfHeight()+50,100);
    asBalas.push(umaBala);
    entities.push(umaBala);
  	GameSounds.TIRO.banheira.play(false, 1);
  }


  
  for (var i=0; i< entities.length;i++){
    entities[i].update();
  }
  render();

	
  checkColisions();
  clearArrays();
  animationHandler=requestAnimationFrame(update, canvas);
  }
}



function clearArrays(){
	entities=entities.filter(filterByActiveProp);
	asBalas=asBalas.filter(filterByActiveProp);
	asBalasInimigos=asBalasInimigos.filter(filterByActiveProp);
	inimigos=inimigos.filter(filterByActiveProp);
}

function filterByActiveProp(obj){
	if (obj.active == true) return obj;
}

function render(){

//  demulF++;
  //if(demulF%2!=0)return;
  //Clear the previous animation frame
  drawingSurface.clearRect(0, 0, canvas.width, canvas.height);

  for(var i=0; i<entities.length; i++){

    var entity=entities[i];
	var sprite= entities[i].getSprite();

  	if(entity.active){
  	  drawingSurface.drawImage
  	  	  (
  	  		entity.spriteSheet.img,
  	  		sprite.x, sprite.y,
  	  		sprite.width, sprite.height,
  	  		entity.x, entity.y,
  	  		entity.width, entity.height
  	  	  );
  	  if(debugMode) entity.drawColisionBoundaries(drawingSurface,false,true, "red","red");
  	}
  }
  displayLevel.render();
  displayScore.render();
  cancelAnimationFrame(animationHandler);
}
