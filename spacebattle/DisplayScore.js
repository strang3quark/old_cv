var DisplayScore = Component.extend(function(){
  var _this=this;
  var fillWith=100;
  this.text="";
  var borderColor="white";
  var fillColor="red";
  var textColor="blue";
  var field="";
  this.constructor= function(x,y,w,h,drawContext,_field,_text, _textColor, _borderColor,_fillColor){
	  this.super();
	  this.x=x;
	  this.y=y;
	  this.width=w;
	  this.height=h;
	  this.ctx=drawContext;
	  borderColor=_borderColor!=undefined?_borderColor:"white";
	  fillColor=_fillColor!=undefined?_fillColor:"red";
	  textColor=_textColor!=undefined?_textColor:"black";
	  field = _field;
	  this.text = field+": "+_text;
  };
  
 this.setValue = function(value){
	  _this.text=field+": "+value;
  };
  
  this.render=function(){
	  this.ctx.clearRect(this.x-40, this.y-40  ,  this.width+80,this.height+80);
	  
	  this.ctx.save();  

	  this.ctx.fillStyle = textColor;
	  this.ctx.font = "35px Arial "+textColor;
	  this.ctx.fillText(this.text,this.left()+2,this.height-(this.height/6));
	   
   
  };
  
  function getGradient(){
	  var grd = _this.ctx.createLinearGradient(_this.x,_this.y,_this.x, _this.y+_this.height);
	  grd.addColorStop(0,borderColor);
	  grd.addColorStop(0.2,borderColor);
	  grd.addColorStop(0.5,"white");
	  grd.addColorStop(0.8,borderColor);
      grd.addColorStop(1,borderColor); 
	  return grd; 
  }
  
  function drawRoundRectangle (xx,yy, ww,hh, rad, fill, strokeWidth, stColor) {
      if (typeof(rad) == "undefined") rad = 5;
       _this.ctx.beginPath();
       _this.ctx.moveTo(xx+rad, yy);
       _this.ctx.arcTo(xx+ww, yy,    xx+ww, yy+hh, rad);
       _this.ctx.arcTo(xx+ww, yy+hh, xx,    yy+hh, rad);
       _this.ctx.arcTo(xx,    yy+hh, xx,    yy,    rad);
       _this.ctx.arcTo(xx,    yy,    xx+ww, yy,    rad);
       _this.ctx.lineWidth = strokeWidth;
	   _this.ctx.fillStyle = fill;
	   _this.ctx.strokeStyle = stColor;
	   if (strokeWidth) _this.ctx.stroke();  // Default to no stroke
       if(fill || typeof(fill)=="undefined") _this.ctx.fill();  // Default to fill
	  
  }; // end of fillRoundedRect method

});


