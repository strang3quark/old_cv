var BalaE = Entity.extend(function(){

  var _this=this;
  this.exploding=false;

  this.states={
	  ATIVO:'ovo'
  }


  this.constructor= function(spriteSheet,x,y,vx,tamanho){
	  this.super();
	  this.spriteSheet=spriteSheet; // spriteSheet
	  this.x=x;  //posX inicial
	  this.y=y;  // posY inicial
	  this.currentState=this.states.ATIVO;  //estado inicial
	  this.currentFrame=0;  //frame inicial
	  this.vx=vx;
	  this.vy=-1;
	  this.tamanho = tamanho;

	  setup();

  };

  this.update=function(){
	  if(!this.active)return;

	  this.x-=this.vx;
	  this.vx -=this.vx>0?0.005:0;

	  this.y-=this.vy;

	  this.width=Math.floor(this.frames[this.currentFrame].width*this.scaleFactor);
	  this.height=Math.floor(this.frames[this.currentFrame].height*this.scaleFactor);

	  this.scale(this.tamanho);
	  if(this.currentFrame<this.frames.length-1) this.currentFrame++;
	  else this.currentFrame=0;
  };

  this.getSprite=function(){
	  return this.frames[this.currentFrame];

  };


  function setup(){
	_this.eStates[_this.states.ATIVO]=
	            _this.spriteSheet.getStats('ovo');

  _this.frames=_this.eStates[_this.currentState];
  _this.width=_this.frames[0].width;
  _this.height=_this.frames[0].height;
 }


 function toogleState(theState){
	  if(_this.currState!=theState){
		_this.currState=theState;
		_this.frames=_this.eStates[theState];
		_this.currentFrame=0;
	  }
  }

});


