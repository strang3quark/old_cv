var Banheira = Entity.extend(function(){
  this.currState=undefined; // estado atual;
  var _this=this;
  this.podeDisparar=false;
  this.states={
	  LEFT:'left',
	  RIGHT:'right',
	  UP:'up',
	  DOWN:'down'
  }


  this.banheiraUpdate=true;

  this.constructor= function(spriteSheet,x,y){
	  this.super();
	  this.x=x;
	  this.y=y;
	  this.spriteSheet=spriteSheet;
	  this.currState=this.states.RIGHT;
	  this.currentFrame=0;
	  setup();
  };

  this.update=function(){
	if (this.killed == true){
		this.y += 15;
		this.x -= 6;
	}
		

	this.currentFrame=this.currentFrame< this.frames.length-1?this.currentFrame+1:0;
	  
	this.width=this.frames[this.currentFrame].width;    //atualizar a altura
	this.height=this.frames[this.currentFrame].height;  // atualizar os
	
	this.scale(0.7);
	
  };

  this.getSprite=function(){
	  return this.frames[this.currentFrame];
  };


   function setup(){

	  _this.eStates['up']=_this.spriteSheet.getStats('up');
	  _this.eStates['down']=_this.spriteSheet.getStats('down');
	  _this.eStates['left']=_this.spriteSheet.getStats('left');
	  _this.eStates['right']=_this.spriteSheet.getStats('right');

	  _this.frames=_this.eStates[_this.currState];
	  _this.width=_this.frames[0].width;  //atualizar a altura
	  _this.height=_this.frames[0].height;  // atualizar os
  };


  this.goLeft=function(){
    toogleState(this.states.LEFT);
  }
  this.goRight=function(){
	 toogleState(this.states.RIGHT);
  }
  this.goUp=function(){
    toogleState(this.states.UP);
  }
  this.goDown=function(){
    toogleState(this.states.DOWN);
  }
  this.fall=function(){
	  toogleState(this.states.RIGHT);
	  this.killed=true;
  }

  function toogleState(theState){
	  if(_this.killed){
		  return;
	  } 
	  if(_this.currState!=theState){
		_this.currState=theState;
		_this.frames=_this.eStates[theState];
		_this.currentFrame=0;
	  }
  }




  //correcoes para melhorar o comportamento das colisoes
  this.right 	= function (){return this.x + this.width;}
  this.bottom	= function (){return this.y+ this.height;}
  this.getCenterX = function(){ return this.x + (this.width * 0.5) -45;} 
  this.getCenterY = function(){ return this.y + (this.height * 0.5) +35;}
  this.getHalfWidth= function() {return this.width * 0.5 -25;}
  this.getHalfHeight= function() {return this.height * 0.5 -35;}

}
);


