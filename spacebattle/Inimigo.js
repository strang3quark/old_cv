var Inimigo = Entity.extend(function(){
  this.currState=undefined; // estado atual;
  var _this=this;
  this.podeDisparar=false;
  this.states={
	  NORMAL:'NORMAL',
	  OPEN:'OPEN'
  }
  
 
  this.constructor= function(spriteSheet,x,y,vx,tamanho){
	  this.super();
	  this.x=x;
	  this.y=y;
	  this.spriteSheet=spriteSheet;
	  this.currState=this.states.NORMAL;
	  this.currentFrame=0;
	  this.vx = vx;
	  this.tamanho = tamanho;
	  setup();
  };
  
  this.update=function(){
	  this.currentFrame=this.currentFrame< this.frames.length-1?this.currentFrame+1:0;
	  this.width=this.frames[this.currentFrame].width;    //atualizar a altura 
	  this.height=this.frames[this.currentFrame].height;  // atualizar os 
	  
	  if (this.killed == false){
		  this.x -= this.vx;
	  }else{
		  this.y += 10;
		  this.x += this.vx*5;
		  if(this.tamanho>0.2){this.tamanho -= 0.05;}
	  }
	  this.scale(this.tamanho);
  };
  
  this.getSprite=function(){
	  return this.frames[this.currentFrame];
  };
  

   function setup(){
	 
	  _this.eStates['NORMAL']=_this.spriteSheet.getStats('normal');
	  _this.eStates['OPEN']=_this.spriteSheet.getStats('open');
	  
	  _this.frames=_this.eStates[_this.currState]; 
	  _this.width=_this.frames[0].width;  //atualizar a altura 
	  _this.height=_this.frames[0].height;  // atualizar os 
	 
	  //animacao disparar, o intervalo diminui consoante o nivel
	  var intervalo = nivel*1000/4;
	  intervalo = (intervalo<750)?750:intervalo; //intervalo nunca menor que 750ms
	  setInterval(_this.disparar, intervalo);
  }



  this.disparar=function(){
  /*Animacao disparar*/
	  if (this.killed){
		  this.podeDisparar=false;
		  return
	  }
	  this.podeDisparar=true;
	  toogleState(this.states.OPEN);
	  setTimeout(this.normal, 100);
  }
  
  this.normal=function(){
	  this.podeDisparar=false;
	  toogleState(this.states.NORMAL);
  }
  
  
  function toogleState(theState){
	  if(_this.killed) return;
	  if(_this.currState!=theState){
		_this.currState=theState;
		_this.frames=_this.eStates[theState];
		_this.currentFrame=0;
	  } 
  }
  
});


